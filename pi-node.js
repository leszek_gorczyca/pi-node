const WebSocket = require('ws');
const Gpio = require('onoff').Gpio;
const wss = new WebSocket.Server({port: 8080});

var io = [];

// funkcja wywolywana w monencie zmiany stanu na wejsciu i
global.plc_callback = function(value, i) {
	var message = {
		i: i,
		value: value
	};
	wss.clients.forEach(function each(client) {
		client.send(JSON.stringify(message));
	});

}

// tworzy funkcje wywolywana w momencie zmiany stanu na wejsciu o indeksie i
function create_callback(i) {
	var fn = 'global.plc_callback(value, '+i+');';
	return Function('error', 'value', fn);
}

// funkcja konfiguruje porty GPIO
function plc_config(plc_gpio, plc_gpio_direction) {
	io = []; // wyczyszczenie tablicy konfiguracji
	for(var i = 0; i<plc_gpio.length; i++) {
		var pin = plc_gpio[i];
		var direction = plc_gpio_direction[i];
		switch (direction) {
			case "in": // wejscie
				var last = io.push(new Gpio(pin, "in", "both", {debounceTimeout: 10})); // konfiguruje GPIO jako wejscie		
				io[last-1].watch(create_callback(i)); //przypisuje funkcje oblugi wejscia
			break;
			case "out": // wyjscie
				io.push(new Gpio(pin, "out"));
			break;
		}
	}
	
}

//akcja wywolywana po polaczeniu
wss.on('connection', function(ws) {
	//akcja wywolywana po otrzymaniu wiadomosci
	ws.on('message', function(message_string) {
		var message = JSON.parse(message_string);
		switch (message.type) {
			case "config": //jesli otrzymano komende konfiguracji
				plc_config(message.gpio, message.gpio_direction);
			break;
			
			case "value": //jesli otrzymano komende ustawienia wartosci
				io[message.gpio].write((message.gpio_value===true)?1:0, function(){});
			break
		}
		
	});
});

process.on('SIGINT', function () {
	//zwolnienie zasobow
	for (i=0; i<io.length; i++) {
		io[i].unexport();
	}
	
	//zakonczenie programu
	process.exit();
});

